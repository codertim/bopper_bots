
	var ctx = null;
	var activeRobots = [];
	var canvasHeight = 600;
	var canvasWidth  = 600;
	var robotCeiling = 30;   // how far robot can get from top of screen before changing direction


	function relativeX(ev) {
		var canvas = document.getElementById('theCanvas');
		// var relX = ev.clientX - ev.target.offsetLeft;
		// var relX = ev.pageX - canvas.offsetLeft;
		var relX = ev.pageX - ev.target.offsetLeft;
		return relX;
	}


	function relativeY(ev) {
		var relY = ev.pageY - ev.target.offsetTop;
		return relY;
	}



	// collision detection for clicking on a robot
	function wasRobotClicked(relX, relY, robot) {
		var isClicked = false;
		var defaultRobotHeight = 100;
		var defaultRobotWidth  = 80;
		var isBetweenXs = false;
		var isBetweenYs = false;
		//alert("event: (" + relX + "," + relY + ")"   + 
		//      "  robot #" + robot.identifier + ": (" + robot.posX+ "," + robot.posY + ")");

		var leftSide   = robot.posX - (defaultRobotWidth/2);
		// alert("leftSide = " + leftSide);
		var rightSide  = robot.posX + (defaultRobotWidth/2);
		// alert("rightSide = " + rightSide);
		var topSide    = robot.posY - (defaultRobotHeight/2);
		// alert("topSide = " + topSide);
		var bottomSide = robot.posY + (defaultRobotHeight/2);
		// alert("bottomSide = " + bottomSide);

		if( (relX > leftSide) && (relX < rightSide)) {
			isBetweenXs = true;
		}

		if( (relY < bottomSide) && (relY > topSide)) {
			isBetweenYs = true;
		}

		if( isBetweenXs && isBetweenYs ) {
			isClicked = true;
			// alert("!!!!!!!!!!!!!!!! Collision detection - robot was clicked!");
			robot.clicked();
		}

		return isClicked;	
	}



	// handle "onlick" event listener for canvas
	function canvasWasClicked(ev) {
		// alert("Clicked at x=" + ev.pageX);
		// alert("Clicked at clientX=" + ev.clientX);
		// alert("Clicked at screenX=" + ev.screenX);
		// alert("offsetLeft = " + ev.target.offsetLeft);
		var relX = relativeX(ev);
		var relY = relativeY(ev);
		// alert("Relative X = " + relX + "     relative Y = " + relY);

		for(var i = 0; i < activeRobots.length; i++) {
			// alert("Game area clicked - checking robot# " + i);   // canvas clicked
			if(activeRobots[i]) {
				// alert("Robot #" + i + " OK");
				if(wasRobotClicked(relX, relY, activeRobots[i])) {
					break;
				}
			} else {
				alert("Robot # " + i + " error");
			}
		}
	}



	function drawRobot1(theContext, posX, posY, counterPersonality) {
		var heightBody  = 30;
		var widthBody   = 60;
		var heightNeck  = 20;
		var widthNeck   = 4;
		var heightHead  = 35;
		var widthHead   = 16;
		var eyeRadius   = 2;
		var wheelRadius = 10;
		var mainColor   = "#ff794b";   // orange - for body and head
		var neckColor   = "#333";
		var feetAndEyes = "#0048d8";
		// alert("Drawing robot1");
		// alert("Drawing robot1 - this.posX = " + posX);


		// feet 
		theContext.fillStyle = feetAndEyes;
		theContext.beginPath();   // left wheel
		theContext.arc(posX - (widthBody/2) + 12, posY + (heightNeck/2) + heightBody + 10, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();
		theContext.beginPath();   // right wheel
		theContext.arc(posX + (widthBody/2) - 12, posY + (heightNeck/2) + heightBody + 10, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();

		// body
		theContext.fillStyle = mainColor;
		theContext.fillRect(posX - (widthBody/2), posY + (heightNeck/2) , widthBody, heightBody);

		// neck
		theContext.fillStyle = neckColor;
		theContext.fillRect(posX - (widthNeck/2), posY - (heightNeck/2) , widthNeck, heightNeck);

		// head
		theContext.fillStyle = mainColor;
		theContext.fillRect(posX - (widthHead/2), posY - (heightNeck/2) - heightHead + counterPersonality, widthHead, heightHead);

		// eyes 
		theContext.fillStyle = feetAndEyes;
		theContext.beginPath();   // left eye
		theContext.arc(posX - (widthHead/2) + 3, posY - (heightNeck/2) - 10 + counterPersonality, eyeRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();
		theContext.beginPath();   // right eye
		theContext.arc(posX + (widthHead/2) - 3, posY - (heightNeck/2) - 10 + counterPersonality, eyeRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();

		// bopper
		theContext.fillStyle = "#ddd";   // off-white
		theContext.beginPath();
		theContext.moveTo(posX - (widthBody/2) - 2, posY + (heightNeck/2) + (heightBody/2) + 15);
		theContext.lineTo(posX + (widthBody/2) + 2, posY + (heightNeck/2) + (heightBody/2) + 15);
		theContext.arcTo(posX, posY + (heightNeck/2) + heightBody + 17,   posX - (widthBody/2) - 2, posY + (heightNeck/2) + (heightBody/2) + 15, 15);
		theContext.closePath();
		theContext.fill();
	}



	function drawRobot2(theContext, posX, posY) {
		var heightBody  = 50;
		var widthBody   = 30;
		var heightNeck  = 10;
		var widthNeck   = 4;
		var heightHead  = 15;
		var widthHead   = 50;
		var eyeRadius   = 3;
		var wheelRadius = 10;
		var mainColor   = "#00A0B0";
		var neckColor   = "#EDC951";
		var feetAndEyes = "#CC333F";
		// alert("Drawing robot1");
		// alert("Drawing robot1 - this.posX = " + posX);

		// feet 
		theContext.fillStyle = feetAndEyes;
		/*
		theContext.beginPath();   // left wheel
		theContext.arc(posX - (widthBody/2) + 12, posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();
		*/
		theContext.beginPath();   // right wheel
		theContext.arc(posX, posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();

		// body
		theContext.fillStyle = mainColor;
		theContext.fillRect(posX - (widthBody/2), posY + (heightNeck/2) , widthBody, heightBody);

		// neck
		theContext.fillStyle = neckColor;
		theContext.fillRect(posX - (widthNeck/2), posY - (heightNeck/2) , widthNeck, heightNeck);

		// head
		theContext.fillStyle = mainColor;
		theContext.fillRect(posX - (widthHead/2), posY - (heightNeck/2) - heightHead , widthHead, heightHead);

		// eyes 
		theContext.fillStyle = feetAndEyes;
		theContext.beginPath();   // left eye
		theContext.arc(posX - (widthHead/2) + 4, posY - (heightNeck/2) - 10, eyeRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();
		theContext.fillStyle = feetAndEyes;
		theContext.beginPath();   // right eye
		theContext.arc(posX + (widthHead/2) - 4, posY - (heightNeck/2) - 10, eyeRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fill();

		// bopper
		theContext.fillStyle = "#ddd";   // off-white
		theContext.fillRect(posX - (widthBody/2) - 1, posY + (heightNeck/2) + (heightBody/2) + 10,     (widthBody + 2), (heightBody/2 - 10));
	}



	function drawRobotExploding(theContext, posX, posY, counterExploding) {
		var offsetX = (counterExploding * 10) + 10;
		var offsetY = (counterExploding * 20) + 10;
		theContext.fillStyle = "#ff0000";
		theContext.beginPath();
		theContext.moveTo(posX - offsetX, posY);
		theContext.lineTo(posX - 1, posY - 3);
		theContext.lineTo(posX + 8, posY - offsetY);
		theContext.lineTo(posX + 12, posY + 2);
		theContext.lineTo(posX + offsetX + 20, posY + offsetY);
		theContext.lineTo(posX + 5, posY + offsetY + 5);
		theContext.lineTo(posX - 2, posY + offsetY + 20);
		theContext.lineTo(posX -2, posY + offsetY);
		theContext.lineTo(posX - offsetX, posY);
		theContext.closePath();
		theContext.fill();
	}


	function drawRobot3(theContext, posX, posY, counterPersonality) {
		var heightBody  = 50;
		var widthBody   = 30;
		var heightNeck  = 10;
		var widthNeck   = 4;
		var heightHead  = 30;
		var widthHead   = 50;
		// var eyeRadius   = 6;
		var eyeWidth    = 15;
		var eyeHeight   = 6;
		var wheelRadius = 4;
		var mainColor   = "#CC333F";  // body and head
		var neckColor   = "#EB6841";
		var feetAndEyes = "#6A4A3C";
		// alert("Drawing robot1");
		// alert("Drawing robot1 - this.posX = " + posX);

		// feet 
		theContext.beginPath();   // center wheel
		theContext.arc(posX, posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fillStyle = feetAndEyes;
		theContext.fill();

		theContext.beginPath();   // left wheel
		theContext.arc(posX - (widthBody * 0.4), posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fillStyle = feetAndEyes;
		theContext.fill();

		theContext.beginPath();   // right wheel
		theContext.arc(posX + (widthBody * 0.4), posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fillStyle = feetAndEyes;
		theContext.fill();

		// body
		theContext.fillStyle = mainColor;
		theContext.fillRect(posX - (widthBody/2), posY + (heightNeck/2) , widthBody, heightBody);

		// neck
		theContext.fillStyle = neckColor;
		theContext.fillRect(posX - (widthNeck/2), posY - (heightNeck/2) , widthNeck, heightNeck);

		// head
		theContext.beginPath();
		theContext.moveTo(posX - (widthHead/2), posY - (heightNeck/2) - heightHead);
		theContext.lineTo(posX + (widthHead/2), posY - (heightNeck/2) - heightHead);
		theContext.bezierCurveTo(posX + (widthHead/2), posY - (heightNeck/2) + 10,     posX - (widthHead/2), posY - (heightNeck/2) + 10,     posX - (widthHead/2), posY - (heightNeck/2) - heightHead);
		theContext.closePath();
		theContext.fillStyle = mainColor;
		theContext.fill();

		// eyes
		theContext.fillStyle = "#333";
		theContext.fillRect(posX - (widthHead/2) + 5, posY - (heightNeck/2) - heightHead + 5, eyeWidth, eyeHeight);
		theContext.fillStyle = "#f00";
		theContext.fillRect(posX - (widthHead/2) + 5 + counterPersonality, posY - (heightNeck/2) - heightHead + 5, 2, eyeHeight);

		theContext.fillStyle = "#333";
		theContext.fillRect(posX + 5                , posY - (heightNeck/2) - heightHead + 5, eyeWidth, eyeHeight);
		theContext.fillStyle = "#f00";
		theContext.fillRect(posX + 5 + counterPersonality, posY - (heightNeck/2) - heightHead + 5, 2, eyeHeight);
		// theContext.beginPath();   // left eye
		// theContext.arc(posX - (widthHead/2) + 14, posY - (heightNeck/2) - 18, eyeRadius, 0, Math.PI * 2, false);
		// theContext.closePath();
		// theContext.fill();

		// bopper
		theContext.fillStyle = "#ddd";   // off-white
		theContext.fillRect(posX - (widthBody/2) - 1, posY + (heightNeck/2) + (heightBody/2) + 10,     (widthBody + 2), (heightBody/2 - 10));
	}



	function drawRobot4(theContext, posX, posY, counterPersonality) {
		var heightBody  = 120;
		var widthBody   = 10;
		var heightNeck  = 10;
		var widthNeck   = 4;
		var heightHead  = 10;
		var widthHead   = 11;
		// var eyeRadius   = 6;
		var eyeWidth    = 2;
		var eyeHeight   = 2;
		var wheelRadius = 4;
		var mainColor   = "#FFFF33";  // body and head
		var neckColor   = "#EB6841";
		var hatColor    = "#FFA500";
		var feetAndEyes = "#333333";
		// alert("Drawing robot1");
		// alert("Drawing robot1 - this.posX = " + posX);

		// feet 
		theContext.beginPath();   // center wheel
		theContext.arc(posX, posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fillStyle = feetAndEyes;
		theContext.fill();

		// wheel
		theContext.beginPath();   // left wheel
		// theContext.arc(posX - (widthBody * 0.4), posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.arc(posX, posY + (heightNeck/2) + heightBody + 3, wheelRadius, 0, Math.PI * 2, false);
		theContext.closePath();
		theContext.fillStyle = "#ff0000";
		// theContext.fillStyle = feetAndEyes;
		theContext.fill();

		// body
		theContext.fillStyle = mainColor;
		theContext.fillRect(posX - (widthBody/2), posY + (heightNeck/2) , widthBody, heightBody);

		// neck
		// theContext.fillStyle = neckColor;
		// theContext.fillRect(posX - (widthNeck/2), posY - (heightNeck/2) , widthNeck, heightNeck);

		// head
		theContext.beginPath();
		theContext.fillStyle = mainColor;
		theContext.moveTo(posX - (widthHead/2), posY + 10);
		theContext.lineTo(posX + (widthHead/2), posY + 10);
		theContext.bezierCurveTo(posX + (widthHead/2),  posY - heightHead,  
                                                       posX - (widthHead/2),  posY - heightHead,  
                                                       posX - (widthHead/2),  posY);
		theContext.closePath();
		theContext.fill();

		// hat
		theContext.beginPath();
		theContext.fillStyle = hatColor;
		theContext.moveTo(posX, posY - (heightNeck/2) - heightHead - 10);
		theContext.lineTo(posX - (widthHead/2) - 2, posY - (heightNeck/2) + 1);
		theContext.lineTo(posX + (widthHead/2) + 2, posY - (heightNeck/2) + 1);
		theContext.closePath();
		// theContext.fillStyle = hatColor;
		theContext.fill();

		// eyes
		theContext.fillStyle = "#333";
		//// theContext.fillRect(posX - (widthHead/2) + 3, posY - heightHead + 3, eyeWidth, eyeHeight);
		theContext.fillStyle = "#f00";
		/// theContext.fillRect(posX - (widthHead/2) + 3 + counterPersonality, posY - (heightNeck/2) - heightHead + 3, 2, eyeHeight);
		theContext.fillRect(posX - 3, posY - heightHead + 8, 2, eyeHeight);

		theContext.fillStyle = "#333";
		/// theContext.fillRect(posX + 3, posY - heightHead + 3, eyeWidth, eyeHeight);
		theContext.fillStyle = "#f00";
		//// theContext.fillRect(posX + 3 + counterPersonality, posY - (heightNeck/2) - heightHead + 5, 2, eyeHeight);
		theContext.fillRect(posX + 1, posY - heightHead + 8, 2, eyeHeight);
		// theContext.beginPath();   // left eye
		// theContext.arc(posX - (widthHead/2) + 14, posY - (heightNeck/2) - 18, eyeRadius, 0, Math.PI * 2, false);
		// theContext.closePath();
		// theContext.fill();

		// bopper
		theContext.fillStyle = "#ddd";   // off-white
		theContext.fillRect(posX - (widthBody/2) - 1, posY + (heightNeck/2) + (heightBody/2) + 10,     (widthBody + 2), (heightBody/2 - 10));
	}



	// collision counter prevents allows robots grace time to separate without
	// immediately triggering another collision
	function updateCollisionCounter(robot) {
		// possible button sounds 11, 22, 24
		if(robot.counterCollided > 15) {
			// reset to non-collided state
			robot.counterCollided = 0;
			// alert("RESET TO NON-COLLIDED STATE");
		} else if(robot.counterCollided > 0) {
			// increment counter
			robot.counterCollided += 1;
		}
	}


	var robot1 = new Robot(1, 75, 75);
	robot1.counterPersonality = 0;     // counter for individual behaviour - move head up and down with this counter
	robot1.directionPersonality = -1;
 	robot1.drawSelf = function(theContext) {
			console.log("robot 1 - counter exploding = " + this.counterExploding);
			if(this.counterExploding > 5) {
				// do not draw, already exploded
				this.isAlive = false;
			} else if(this.counterExploding > 0) {
				// in process of exploding
				this.counterExploding++;
				drawRobotExploding(theContext, this.posX, this.posY, this.counterExploding);
			} else {
				drawRobot1(theContext, this.posX, this.posY, this.counterPersonality);
			}
	};

	robot1.updatePosition = function() {
			this.counterPersonality += (1 * this.directionPersonality);

			if(this.counterPersonality > 10) {
				this.directionPersonality *= -1;
				// this.counterPersonality = 0;
			} else if(this.counterPersonality < 0) {
				this.directionPersonality *= -1;
			}

			if(this.isInInteraction) {
				// do nothing, let interaction update position
			} else if(this.counterCollided === 1) {
				// alert("Robot 1 just collided");	
				if(Math.random() < 0.3) { 
					this.changeDirection();
				} else {
					this.explode();
				}
			} else {
				this.posX += 5 * this.horizontalDirection;
				this.posY += 1 * this.verticalDirection;
				if(this.posX < 30) {
					this.horizontalDirection = 1;
				} else if(this.posX > (canvasHeight-50)) {
					this.horizontalDirection = -1;
				}
				if(this.posY < robotCeiling) {
					this.verticalDirection = 1;
				} else if(this.posY > (canvasWidth-50)) {
					this.verticalDirection = -1;
				}
			}

			updateCollisionCounter(this);
	};



	var robot2 = new Robot(2, 325, 125);
	robot2.drawSelf = function(theContext) {
			console.log("robot " + this.identifier + " - counter exploding = " + this.counterExploding);
			if(this.counterExploding > 5) {
			// do not draw, already exploded
				this.isAlive = false;
			} else if(this.counterExploding > 0) {
				// in process of exploding
				this.counterExploding++;
				drawRobotExploding(theContext, this.posX, this.posY, this.counterExploding);
			} else {
				drawRobot2(theContext, this.posX, this.posY, this.counterPersonality);
			}
	};
	robot2.updatePosition = function() {
			if(this.isInInteraction) {
				// do nothing, let interaction update position
			} else if(this.counterCollided === 1) {
				// alert("Robot 2 just collided");	
				if(Math.random() < 0.3) { 
					this.changeDirection();
				} else {
					this.explode();
				}
			} else {
				this.posX += 0 * this.horizontalDirection;
				this.posY += 2 * this.verticalDirection;
				if(this.posX < 30) {
					this.horizontalDirection = 1;
				} else if(this.posX > (canvasHeight-50)) {
					this.horizontalDirection = -1;
				}
				if(this.posY < robotCeiling) {
					this.verticalDirection = 1;
				} else if(this.posY > (canvasWidth-50)) {
					this.verticalDirection = -1;
				}
			}

			updateCollisionCounter(this);
	};


	function Robot(identifier, posX, posY) {
		// params
		this.identifier = identifier;
		this.posX       = posX + Math.floor(Math.random()*50);   // allow some randomness in start pos
		this.posY       = posY + Math.floor(Math.random()*50);   // allow some randomness in start pos

		// variables not set through constructor params
		this.counterCollided  = 0;
		this.counterExploding = 0;
		this.isAlive          = true;
		this.isInInteraction  = false;
		if(Math.random() < 0.5) {
			this.horizontalDirection = -1;
		} else {
			this.horizontalDirection = 1;
		}
		if(Math.random() < 0.5) {
			this.verticalDirection = -1;
		} else {
			this.verticalDirection = 1;
		}
	}

	Robot.prototype.changeDirection = function() {
			// alert("In Robot#changeDirection section");
			this.horizontalDirection *= -1;
			this.verticalDirection *= -1;
	};

	Robot.prototype.explode = function() {
			// alert("In Robot#explode section - robot#" + this.identifier);
			if(this.counterExploding < 1) {
				this.counterExploding = 1;
				var robotSound = new Audio("sounds/explosion-01.wav");
				robotSound.play();
			}
	}; //end function

	Robot.prototype.clicked = function() {
			// alert("In Robot#clicked section - robot#" + this.identifier);
			this.changeDirection();
			var robotSound = new Audio("sounds/button-11.wav");
			robotSound.play();
	}; //end function



	// Create Robot 3
	var robot3 = new Robot(3, 100, 400);
	robot3.counterPersonality   = 2;     // counter for individual behaviours - move head up and down
	robot3.directionPersonality = 1;
	robot3.drawSelf = function(theContext) {
			console.log("robot " + this.identifier + " - counter exploding value = " + this.counterExploding + "   posX = " + this.posX);
			if(this.counterExploding > 5) {
				// do not draw, already exploded
				this.isAlive = false;
			} else if(this.counterExploding > 0) {
				// in process of exploding
				this.counterExploding++;
				drawRobotExploding(theContext, this.posX, this.posY, this.counterExploding);
			} else {
				drawRobot3(theContext, this.posX, this.posY, this.counterPersonality);
			}
	};  //end function
	robot3.updatePosition = function() {
			console.log("robot 3 - isAlive = " + this.isAlive + "   isInInteraction = " + this.isInInteraction + "   counterCollided=" + this.counterCollided + "   horiszontalDirection=" + this.horizontalDirection);
			if(!this.isAlive) {
				return;
			}

			this.counterPersonality += 1 * this.directionPersonality;
			if(this.counterPersonality > 12) {
				this.directionPersonality = -1;
			} else if(this.counterPersonality < 2) {
				this.directionPersonality = 1;
			}

			if(this.isInInteraction) {
				// do nothing, let interaction update position
			} else if(this.counterCollided === 1) {
				// alert("Robot 3 just collided");
				if(Math.random() < 0.3) { 
					this.changeDirection();
				} else {
					this.explode();
				}
			} else {
				console.log("robot 3 - updating positions in X and Y directions");
				console.log("robot 3 - 2 * this.horizontalDirection=" + (2 * this.horizontalDirection));
				// this.posX += 2 * this.horizontalDirection;
				console.log("robot 3 - this.posX - before = " + this.posX);
				this.posX = this.posX + (2 * this.horizontalDirection);
				console.log("robot 3 - this.posX - after= " + this.posX);
				this.posY += 1 * this.verticalDirection;
				if(this.posX < 30) {
					this.horizontalDirection = 1;
				} else if(this.posX > (canvasHeight-50)) {
					this.horizontalDirection = -1;
				}
				if(this.posY < robotCeiling) {
					this.verticalDirection = 1;
				} else if(this.posY > (canvasWidth-50)) {
					this.verticalDirection = -1;
				}
			}

			updateCollisionCounter(this);
	}; //end function


	// Create Robot 4
	var robot4 = new Robot(4, 400, 325);
	robot4.counterPersonality   = 2;     // counter for individual behaviours - move head up and down with this counter
	robot4.directionPersonality = 1;
	robot4.drawSelf = function(theContext) {
		console.log("robot " + this.identifier + " - counter exploding = " + this.counterExploding);
		drawRobot4(theContext, this.posX, this.posY, this.counterPersonality);
	};
	robot4.updatePosition = function() {
			this.counterPersonality += 1 * this.directionPersonality;
			if(this.counterPersonality > 12) {
				this.directionPersonality = -1;
			} else if(this.counterPersonality < 2) {
				this.directionPersonality = 1;
			}

			if(this.isInInteraction) {
				// do nothing, let interaction update position
			} else if(this.counterCollided === 1) {
				// alert("Robot 4 just collided");	
				this.changeDirection();
			} else {
				this.posX += 2 * this.horizontalDirection;
				this.posY += 1 * this.verticalDirection;
				if(this.posX < 30) {
					this.horizontalDirection = 1;
				} else if(this.posX > (canvasHeight-50)) {
					this.horizontalDirection = -1;
				}
				if(this.posY < 10) {
					this.verticalDirection = 1;
				} else if(this.posY > (canvasWidth-50)) {
					this.verticalDirection = -1;
				}
			}

			updateCollisionCounter(this);
	};


	function testRobotForCollision(robot, otherRobots) {
		var currentOtherRobot = undefined;
		var allowableX        = 40; 
		var allowableY        = 60;

		if(!robot.isAlive) {
			return;
		}

		for(var i = 0 ; i < otherRobots.length; i++) {
			currentOtherRobot = otherRobots[i];	
			var isXOk             = true;
			var isYOk             = true;

			if(!currentOtherRobot.isAlive) {
				return;
			}

			if(currentOtherRobot) {
				var diffX = Math.abs(robot.posX - currentOtherRobot.posX);
				var diffY = Math.abs(robot.posY - currentOtherRobot.posY);
				if( (diffX < allowableX) && (diffY < allowableY) ) {
					// alert("Collision detected at i = " + i);
					robot.counterCollided = 1;
					currentOtherRobot.counterCollided = 1;
					var robotSound = new Audio("sounds/button-19.wav");
					robotSound.play();
				}
			} else {
				alert("Error #313653");
			}
		}
	}


	// collision detection of robots
	function checkForCollisions(theRobots) {
		var currentRobot = undefined;

		for(var i=0; i < theRobots.length; i++) {
			currentRobot = theRobots[i];

			if(currentRobot.counterCollided > 0) {
				continue;
			}

			if(currentRobot && currentRobot.isAlive) {
				// robot is defined at this position in array
				// alert("robot index i = " + i + "   currentRobot.posX = " + currentRobot.posX + "   currentRobot.posY = " + currentRobot.posY);
				if( testRobotForCollision(currentRobot, theRobots.slice(i+1)) ) {
					break;
				}
			} else {
				// empty robot value at this position in array
				alert("Robot Undefined - index " + i);
			}
		}
	}


	function updateObjects(ctx) {
		console.log("updateObjects starting ... activeRobots.length=" + activeRobots.length);
	
		// Update Positions
		for(var i in activeRobots) {
			var robot = activeRobots[i];
			if(robot.isAlive) {
				robot.updatePosition();
			}
		}

		// Draw Robots
		for(var i in activeRobots) {
			var robot = activeRobots[i];
			if(robot.isAlive) {
				robot.drawSelf(ctx);
			}
		}

		checkForCollisions(activeRobots);
	}


	function numActiveRobots(robots) {
		var count = 0;

		for(var i in robots) {
			if(robots[i].isAlive) {
				count++;
			}
		}

		return count;
	}


	function doesActiveRobotsHaveDeadRobot(robots) {
		if(numActiveRobots(robots) != robots.length) {
			return true;
		} else {
			return false;
		}
	}


	// remove dead robots by copying over active robots only
	function removeDeadRobots() {
		if(doesActiveRobotsHaveDeadRobot(activeRobots)) {
			var latestRobotsVerifiedActive = [];

			for(var i in activeRobots) {
				var robot = activeRobots[i];
				if(robot.isAlive) {
					latestRobotsVerifiedActive.push(robot);
				} else {
					console.log("Removing dead robot #" + robot.identifier);
				}
			}

			activeRobots.length = 0;   // clear old values
			activeRobots = latestRobotsVerifiedActive;
		}
	}


	// this is the "main"
	function gameLoop(ctx) {
		ctx.clearRect(0, 0, canvasWidth, canvasHeight);
		removeDeadRobots();
		updateObjects(ctx);

		setTimeout(function() {
			gameLoop(ctx);
		}, 100);
	}


	$(document).ready(function() {
		// alert("Starting ..."); 
		var theCanvas = $("#theCanvas");
		var context = theCanvas.get(0).getContext("2d");
		<!-- context.drawImage(".", 10, 10); -->
		ctx = context;
		// alert("ctx = " + ctx);
		alert("Start");

		// setup robots array
		Array.prototype.push.apply(activeRobots, [robot1, robot2, robot3, robot4]);

		// each robot should start by drawing self
		for(var i in activeRobots) {
			activeRobots[i].drawSelf(context);
		}

		// start animation loop
		gameLoop(ctx);
	});


